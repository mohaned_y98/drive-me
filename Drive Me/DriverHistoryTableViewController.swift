//
//  DriverHistoryTableViewController.swift
//  Drive Me
//
//  Created by Mohaned Al-Feky on 7/21/18.
//  Copyright © 2018 mohaned. All rights reserved.
//

import UIKit
import FirebaseDatabase
import  FirebaseAuth
class DriverHistoryTableViewController: UITableViewController {
    var driverRides = [String]()
    var selected : NSMutableArray = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableViewAutomaticDimension
        if let email = Auth.auth().currentUser?.email{
            Database.database().reference().child("RideRequests").queryOrdered(byChild: "driverEmail").queryEqual(toValue: email).observe(.childAdded) { (snapshot) in
                if  let rideRequestsDicitionary = snapshot.value as? [String:AnyObject]{
                    if let email = rideRequestsDicitionary["email"] as? String {
                        
                        self.driverRides.append(email)
                        
                    }
                self.tableView.reloadData()
                }
                
                Database.database().reference().removeAllObservers()
            }
        
        }
    }

   


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return driverRides.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell", for: indexPath)
        if driverRides[indexPath.row] != ""{
            cell.textLabel?.numberOfLines = 2
        cell.textLabel?.text = "Rider Email : \(driverRides[indexPath.row])\nStatus: Accepted"
        
        }
        cell.textLabel?.textColor = UIColor.white
        return cell
    }
    
   override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if let cell = tableView.cellForRow(at: indexPath){
        cell.accessoryType = .checkmark
        if driverRides[indexPath.row] != ""{
            if !selected.contains(driverRides[indexPath.row]){
            cell.textLabel?.numberOfLines = 2
            cell.textLabel?.text = "Rider Email : \(driverRides[indexPath.row])\nStatus: Done"
            selected.add(driverRides[indexPath.row])
            }else{
                cell.accessoryType = .none
                cell.textLabel?.text = "Rider Email : \(driverRides[indexPath.row])\nStatus: Accepted"
                selected.remove(driverRides[indexPath.row])
            }
        }
        
    }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
