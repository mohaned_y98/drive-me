//
//  AcceptRideViewController.swift
//  Drive Me
//
//  Created by Mohaned Al-Feky on 7/20/18.
//  Copyright © 2018 mohaned. All rights reserved.
//

import UIKit
import MapKit
import FirebaseDatabase
import FirebaseAuth
class AcceptRideViewController: UIViewController {

    @IBOutlet weak var map: MKMapView!
    var requestLocation = CLLocationCoordinate2D()
    var requestEmail = ""
    var driverLocation = CLLocationCoordinate2D()
    override func viewDidLoad() {
        super.viewDidLoad()
        let region = MKCoordinateRegion(center: requestLocation, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        map.setRegion(region, animated: false)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = requestLocation
        annotation.title = requestEmail
        map.addAnnotation(annotation)
        
        
    }

    @IBAction func AcceptPressed(_ sender: UIButton) {
        Database.database().reference().child("RideRequests").queryOrdered(byChild: "email").queryEqual(toValue: requestEmail).observe(.childAdded) { (snapshot) in
            if let driverEmail = Auth.auth().currentUser?.email{
                snapshot.ref.updateChildValues(["driverLat":self.driverLocation.latitude,"driverLon":self.driverLocation.longitude,"driverEmail":driverEmail])
        }
            Database.database().reference().child("RideRequests").removeAllObservers()

        }
        
        let requestCLLocation = CLLocation(latitude: requestLocation.latitude, longitude: requestLocation.longitude)
        
        CLGeocoder().reverseGeocodeLocation(requestCLLocation) { (placemarks, error)
            in
            if let places = placemarks {
                
                if places.count > 0 {
                    
                    let mkPlacemark = MKPlacemark(placemark: places[0])
                    let mapItem = MKMapItem(placemark: mkPlacemark)
                    mapItem.name = self.requestEmail
                    let options = [MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving]
                    mapItem.openInMaps(launchOptions: options)
                    
                }
                
            }
            
            
            
        }
        
        
        
        
    }

}
