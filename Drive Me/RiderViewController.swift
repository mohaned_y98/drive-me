//
//  RiderViewController.swift
//  Drive Me
//
//  Created by Mohaned Al-Feky on 7/20/18.
//  Copyright © 2018 mohaned. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import MapKit
class RiderViewController: UIViewController, CLLocationManagerDelegate {
    @IBOutlet weak var callaDriverButton: UIButton!
    
    @IBOutlet weak var map: MKMapView!
    
    var driverHasBeenCalled = false
    var userLocation = CLLocationCoordinate2D()
    var driverLocation = CLLocationCoordinate2D()
    var driverComing = false
    var locationManger = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManger.delegate = self
        locationManger.desiredAccuracy = kCLLocationAccuracyBest
        locationManger.requestWhenInUseAuthorization()
        locationManger.startUpdatingLocation()
        
        if let email = Auth.auth().currentUser?.email{
            Database.database().reference().child("RideRequests").queryOrdered(byChild: "email").queryEqual(toValue: email).observe(.childAdded) { (snapshot) in
                self.driverHasBeenCalled = true
                self.callaDriverButton.setTitle("CANCEL RIDE", for: .normal)
                if  let rideRequestsDicitionary = snapshot.value as? [String:AnyObject]{
                    if let driverLon = rideRequestsDicitionary["driverLon"] as? Double {
                        if let driverLat = rideRequestsDicitionary["driverLat"] as? Double {
                            self.driverLocation = CLLocationCoordinate2D(latitude: driverLat, longitude: driverLon)
                            self.driverComing = true
                            self.displayDriverAndRider()
                            if let email = Auth.auth().currentUser?.email {
                                Database.database().reference().child("RideRequests").queryOrdered(byChild: "email").queryEqual(toValue: email).observe(.childChanged) { (snapshot) in
                                    if  let rideRequestsDicitionary = snapshot.value as? [String:AnyObject]{
                                        if let driverLon = rideRequestsDicitionary["driverLon"] as? Double {
                                            if let driverLat = rideRequestsDicitionary["driverLat"] as? Double {
                                                self.driverLocation = CLLocationCoordinate2D(latitude: driverLat, longitude: driverLon)
                                                self.driverComing = true
                                                self.displayDriverAndRider()
                                                
                                            }
                                            
                                            
                                        }
                                        
                                    }
                                }
                            }
                            
                        }
                        
                        
                    }
                    
                }
                
                
                Database.database().reference().child("RideRequests").removeAllObservers()
                
            }
            
        }
        
        
    }
    
    func displayDriverAndRider(){
        let driverCLLocation = CLLocation(latitude: driverLocation.latitude, longitude: driverLocation.longitude)
        let riderCLLocation = CLLocation(latitude: userLocation.latitude, longitude: userLocation.longitude)
        let distance =  round((driverCLLocation.distance(from: riderCLLocation) / 1000)*100)/100
        callaDriverButton.setTitle("Driver is \(distance)KM away!", for: .normal)
        map.removeAnnotations(map.annotations)
        let latdelta = abs(driverLocation.latitude - userLocation.latitude) * 2 + 0.005
        let londelta = abs(driverLocation.longitude - userLocation.longitude) * 2 + 0.005
        let region = MKCoordinateRegion(center: userLocation, span: MKCoordinateSpan(latitudeDelta:latdelta, longitudeDelta: londelta))
        map.setRegion(region, animated: true)
        let userAnnotation = MKPointAnnotation()
        userAnnotation.coordinate = userLocation
        userAnnotation.title = "Your Location"
        map.addAnnotation(userAnnotation)
        let driverAnnotation = MKPointAnnotation()
        driverAnnotation.coordinate = driverLocation
        driverAnnotation.title = "Driver Location"
        map.addAnnotation(driverAnnotation)
    }
    
    func  locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let cord = manager.location?.coordinate{
            
            let center = CLLocationCoordinate2D(latitude: cord.latitude, longitude: cord.longitude)
            
            userLocation = center
            
            if driverComing{
                
                displayDriverAndRider()
                if let email = Auth.auth().currentUser?.email {
                    Database.database().reference().child("RideRequests").queryOrdered(byChild: "email").queryEqual(toValue: email).observe(.childChanged) { (snapshot) in
                        if  let rideRequestsDicitionary = snapshot.value as? [String:AnyObject]{
                            if let driverLon = rideRequestsDicitionary["driverLon"] as? Double {
                                if let driverLat = rideRequestsDicitionary["driverLat"] as? Double {
                                    self.driverLocation = CLLocationCoordinate2D(latitude: driverLat, longitude: driverLon)
                                    self.driverComing = true
                                    self.displayDriverAndRider()
                                    
                                }
                                
                                
                            }
                            
                        }
                    }
                }
                    
                
            }else{
                
                let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
                
                map.setRegion(region, animated: true)
                map.removeAnnotations(map.annotations)
                let annotation = MKPointAnnotation()
                annotation.coordinate = center
                annotation.title = "Your Location"
                map.addAnnotation(annotation)
            }
            
            
        }
    }
    
    
    @IBAction func logoutPressed(_ sender: UIBarButtonItem) {
        do{
            
            try Auth.auth().signOut()
            navigationController?.dismiss(animated: true, completion: nil)
        }catch{
            print(error)
        }
    }
    
    @IBAction func callADriverPressed(_ sender: UIButton) {
        if !driverComing{
            if let email = Auth.auth().currentUser?.email {
                if (driverHasBeenCalled == false){
                    let rideRequest : [String:Any] = ["email":email,"lat":userLocation.latitude,"lon":userLocation.longitude]
                    
                    Database.database().reference().child("RideRequests").childByAutoId().setValue(rideRequest)
                    driverHasBeenCalled = true
                    callaDriverButton.setTitle("CANCEL RIDE", for: .normal)
                    let alert = UIAlertController(title: "The Ride Requested", message: "Just Wait !!", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        alert.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                }else{
                    driverHasBeenCalled = false
                    callaDriverButton.setTitle("CALL A DRIVER", for: .normal)
                    Database.database().reference().child("RideRequests").queryOrdered(byChild: "email").queryEqual(toValue: email).observe(.childAdded) { (snapshot) in
                        snapshot.ref.removeValue()
                        Database.database().reference().child("RideRequests").removeAllObservers()
                        
                        
                    }
                    
                    let alert = UIAlertController(title: "The Ride Canceled", message: "", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        alert.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                }
            }
        }else{
            
            displayDriverAndRider()
        }
    }
    
    
    
}
