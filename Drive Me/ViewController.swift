//
//  ViewController.swift
//  Drive Me
//
//  Created by Mohaned Al-Feky on 7/20/18.
//  Copyright © 2018 mohaned. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
class ViewController: UIViewController {
    
    @IBOutlet weak var riderLabel: UILabel!
    @IBOutlet weak var riderDriverSwitch: UISwitch!
    
    @IBOutlet weak var driverLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var switchButton: UIButton!
    
    @IBOutlet weak var signupOrLoginButton: UIButton!
    
    var signUpMode = true
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func displayAlert(title: String,message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func signupPressed(_ sender: UIButton) {
        if emailTextField.text == "" || passwordTextField.text == ""{
            displayAlert(title:"Error",message: "Enter both email and password")
        }else{
            if let email = emailTextField.text {
                if let password = passwordTextField.text{
                    if signUpMode {
                        Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
                            if error != nil {
                                self.displayAlert(title:"Error",message: (error?.localizedDescription)!)
                            }else{
                                if self.riderDriverSwitch.isOn {
                                    //Driver
                                    let req = Auth.auth().currentUser?.createProfileChangeRequest()
                                    req?.displayName = "Driver"
                                    req?.commitChanges(completion: nil)
                                    self.performSegue(withIdentifier: "DriverSegue", sender: self)
                                    
                                }else{
                                    let req = Auth.auth().currentUser?.createProfileChangeRequest()
                                    req?.displayName = "Rider"
                                    req?.commitChanges(completion: nil)
                                    self.performSegue(withIdentifier: "riderSegue", sender: self)
                                    
                                }
                                
                            }
                        }
                        
                        
                    }else{
                        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
                            if error != nil {
                                self.displayAlert(title:"Error",message: (error?.localizedDescription)!)
                            }else{
                                if user?.user.displayName == "Rider"{
                                    self.performSegue(withIdentifier: "riderSegue", sender: self)
                                }else{
                                    //Driver
                                    print("Driver")
                                    self.performSegue(withIdentifier: "DriverSegue", sender: self)
                                }
                                
                            }
                            
                            
                        }
                    }
                }
                
                
            }
            
            
        }
    }
    
    @IBAction func switchButton(_ sender: UIButton) {
        if(signUpMode){
            signupOrLoginButton.setTitle("LOG IN", for: .normal)
            riderLabel.isHidden = true
            driverLabel.isHidden = true
            riderDriverSwitch.isHidden = true
            switchButton.setTitle("SWITCH TO SIGN UP", for: .normal)
            signUpMode = false
            
        }else{
            signupOrLoginButton.setTitle("SIGN UP", for: .normal)
            riderLabel.isHidden = false
            driverLabel.isHidden = false
            riderDriverSwitch.isHidden = false
            switchButton.setTitle("SWITCH TO LOG IN", for: .normal)
            signUpMode = true
            
            
        }
        
    }
    @IBAction func aboutPressed(_ sender: UIButton) {
        let alert = UIAlertController(title: "About", message: "Dev: @Mohaned_y98 ", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Twitter", style: .destructive, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            if let url = URL(string: "https://twitter.com/mohaned_y98") {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
}

