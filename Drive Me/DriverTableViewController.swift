//
//  DriverTableViewController.swift
//  Drive Me
//
//  Created by Mohaned Al-Feky on 7/20/18.
//  Copyright © 2018 mohaned. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import MapKit

class DriverTableViewController: UITableViewController, CLLocationManagerDelegate {
    var locationManger = CLLocationManager()
    var rideRequests : [DataSnapshot] = []
    var driverLocaton = CLLocationCoordinate2D()
    
    @objc func refresh(sender:AnyObject){
        // Updating your data here...
        print("Refreshing..")
        self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
    }
    
    
    override func viewDidLoad() {
      
        super.viewDidLoad()
       
        let attributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        let attributedTitle = NSAttributedString(string: "Refreshing..", attributes: attributes)
        self.refreshControl?.attributedTitle = attributedTitle
        self.refreshControl?.tintColor = UIColor.white
     
        self.refreshControl?.addTarget(self, action: #selector(DriverTableViewController.refresh(sender:)), for: UIControlEvents.valueChanged)
        locationManger.delegate = self
        locationManger.desiredAccuracy = kCLLocationAccuracyBest
        locationManger.requestWhenInUseAuthorization()
        locationManger.startUpdatingLocation()
        
        Database.database().reference().child("RideRequests").observe(.childAdded) { (snapshot) in
            
            if  let rideRequestsDicitionary = snapshot.value as? [String:AnyObject]{
                if let driverLon = rideRequestsDicitionary["driverLon"] as? Double {
                    
                    
                    
                }else{
                    self.rideRequests.append(snapshot)
                    self.tableView.reloadData()
                }
                
            }
           
        }
        
        Timer.scheduledTimer(withTimeInterval: 3, repeats: true) { (timer) in
            self.tableView.reloadData()
        }
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let cord = manager.location?.coordinate{
            driverLocaton = cord
            
        }
        
        
    }
    
    @IBAction func logoutPressed(_ sender: UIBarButtonItem) {
        do{
            
            try Auth.auth().signOut()
            navigationController?.dismiss(animated: true, completion: nil)
        }catch{
            print(error)
        }
    }
    
    
    // MARK: - Table view data source
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return rideRequests.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let snapshot = rideRequests[indexPath.row]
        if  let rideRequestsDicitionary = snapshot.value as? [String:AnyObject]{
            if let email = rideRequestsDicitionary["email"] as? String {
                if let lon = rideRequestsDicitionary["lon"] as? Double {
                    if let lat = rideRequestsDicitionary["lat"] as? Double {
                        let driverCLLocation = CLLocation(latitude: driverLocaton.latitude, longitude: driverLocaton.longitude)
                        
                        let riderCLLocation = CLLocation(latitude: lat, longitude: lon)
                        
                        
                        let distance =  round((driverCLLocation.distance(from: riderCLLocation) / 1000)*100)/100
                        
                        cell.textLabel?.text = "\(email) - \(distance) KM away"
                        
                    }
                    
                }
                
            }
            
        }
        cell.textLabel?.textColor = UIColor.white
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let snapshot = rideRequests[indexPath.row]
        
        performSegue(withIdentifier: "acceptSegue", sender: snapshot)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let acceptVC = segue.destination as? AcceptRideViewController{
            if let snapshot = sender as? DataSnapshot{
                if  let rideRequestsDicitionary = snapshot.value as? [String:AnyObject]{
                    if let email = rideRequestsDicitionary["email"] as? String {
                        if let lon = rideRequestsDicitionary["lon"] as? Double {
                            if let lat = rideRequestsDicitionary["lat"] as? Double {
                                
                                acceptVC.requestEmail = email
                                let location = CLLocationCoordinate2D(latitude: lat, longitude: lon )
                                acceptVC.requestLocation = location
                                acceptVC.driverLocation = driverLocaton
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    @IBAction func historyPressed(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "DriverHistorySegue", sender: self)
    }
    
}
